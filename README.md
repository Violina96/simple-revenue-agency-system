# Simple Revenue Agency System

Development of a Web-application for a simplified version of the Revenue Agency system

# Tools:
* Programming Language:
     - Java (using jdk 11)
* Application Framework:
     - Spring Framework / Spring Boot
* Persistence:
     - Spring Data and Hibernate
     - H2 database
* Presentation:
     - Thymeleaf - templating engine
     - HTML, CSS
* Build:
     - Maven - build automation and dependency management
     
## Testdata
* testdata/data.txt - data for testing the console application

## Info
The application consists of two parts - a console application, whit which we can import data into a database,
and a web application, with which the user can easily manage the data and use the functionality of the application,
by filling in text boxes, navigating through buttons, and printing data into forms in web pages.

## Console Application
In the first line of the console will be entered number 'N' followed by 'N*3' lines, with every three lines representing the "file" of one person. 
Every three lines looks like this:

1. In the first line will be entered the personal data and the address of the person. Data includes:
   * First and last name - Free text
   * Gender - one letter `M` or ` F`
   * Height - a positive integer
   * Date of birth
   * Country, city, municipality, zip code, street name, street number - free text

2. In the second line education data will be entered (if any). In the example, data is given only for one education, 
   but they can also be many, also separated by `;`. Each education contains:
   * Type of education - one letter `P`,` S`, `B`,` M` or `D`
       * P - Primary education
       * S - Secondary education
       * B - Higher Education - Bachelor Degree
       * M - Higher Education - Master Degree
       * D - Higher Education - Doctor Degree
   * Name of institution - free text
   * Start date
   * Date of (possibly) completion
   * Whether the education is completed - `YES` or ` NO`
   * Grade if education is completed or an empty string if the education is primary or incomplete
   
3. The third line will include social security data (if any). In the example, only the data for one social security record is given, 
   but they can also be many, also separated by `;`. Each entry contains:
   * Amount on which the person is insured
   * The year to which the recording relates
   * The month to which the recording refers
        
    *All dates are in format `d.M.yyyy` (пр. `1.2.2003`, `11.1.2014`, `12.12.2012`).*
        
Entry Format:

```
<first_name>;<last_name>;<gender>;<height>;<birthday>;<country>;<city>;<municipality>;<postcode>;<street_name>;<street_number>
<education_type>;<institution_name>;<enrollment_date>;<graduation_date>;<has_graduated>;[<final_grade>]
<amount>;<year>;<month>
```
## Web Application

The web application uses the MVC model and Thymeleaf template engine.
There are two web pages `people.html` and `personDetails.html`. The first one is the "home" page, from which you can navigate to the other. 
In `people.html` there is a list with all people in the database, with only the names being displayed. Both names are links to the page 
`personDetails.html`, where detailed information of the person will be represented. In `people.html` there are also two text fields and
one button, which creates a search form. In the both fields the user is able to enter the first and/or the last name of the person
and the data will be filtered based on these criteria.

List of people in the database: 
* Петър Петров
* Иван Иванов
* Петър Иванов

If the user enters "Петър Иванов", only the third person should appear. If the user enters only the first name for search "Петър",
the search result should be the first and the third person. If only the last name is entered "Иванов", the search result should be 
the second and the third person.

There is also a second button "Add" on the `people.html` page, which when pressed navigates the user to another page `new_person.html`,
where a new person can easily be added.

On the pages `personDetails.html` and `new_person.html`, the user will see a 'Назад' button at the top, that will lead to the first page.

On the last line of the personal details table, there is a button `Стойност на социалните осигуровки`, which when
pressed, must disappear and a text representing the value of the social benefits to which a person
is entitled appears, or 'Няма право на социални помощи' if the person is not entitled to such benefits.

A person is entitled to social aid if:
* has completed secondary education
* has not been insured for the last 3 months.

The value of social benefits is equal to the sum of all social insurances in the last 27 months divided by 24.


