package bg.swift.socialsystem;

import bg.swift.socialsystem.education.*;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.*;

@Service
public class ConsoleRunner implements ApplicationRunner {

    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy");

    private final PersonRepository personRepository;

    public ConsoleRunner(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        scanner.nextLine();

        for (int i = 0; i < n; i++) {
            String line1 = scanner.nextLine();
            Person person = parsePersonLine(line1);

            String line2 = scanner.nextLine();
            Set<Education> education = parseEducation(line2);
            person.setEducation(education);


            String line3 = scanner.nextLine();
            Set<SocialInsuranceRecord> records = parseSocialInsuranceLine(line3);
            person.addSocialInsuranceRecord(records);

            personRepository.save(person);
        }

        System.out.println("People are saved!");
    }


    private static Person parsePersonLine(String line) {

        String[] split = line.split(";");

        String[] personDetails = Arrays.copyOfRange(split, 0, 5);
        Person person = parsePersonDetails(personDetails);

        String[] addressDetails = Arrays.copyOfRange(split, 5, 11);
        Address address = parseAddress(addressDetails);
        person.setAddress(address);

        return person;
    }

    private static Person parsePersonDetails(String[] split) {

        String firstName = split[0];
        String lastName = split[1];
        char gender = split[2].charAt(0);
        int height = Integer.parseInt(split[3]);
        LocalDate birthday = LocalDate.parse(split[4], formatter);

        return new Person(firstName, lastName, Gender.of(gender), height, birthday);
    }

    private static Address parseAddress(String[] split) {

        String country = split[0];
        String city = split[1];
        String municipality = split[2];
        String postcode = split[3];
        String streetName = split[4];
        String streetNumber = split[5];

        return new Address(country, city, municipality, postcode, streetName, streetNumber);
    }


    private static Set<Education> parseEducation(String line) {

        String[] split = line.split(";", -1);

        Set<Education> education = new LinkedHashSet<>();

        if (line.isEmpty()) return education;

        for (int i = 0; i < split.length; i += 6) {

            String institutionType = split[i];
            String institution = split[i + 1];
            LocalDate enrollmentDate = LocalDate.parse(split[i + 2], formatter);
            LocalDate graduationDate = LocalDate.parse(split[i + 3], formatter);
            String isGraduated = split[i + 4];

            Education education1 = chooseEducation(
                    EducationDegree.of(institutionType.charAt(0)),
                    institution,
                    enrollmentDate,
                    graduationDate);

            if (isGraduated.equals("YES")) {
                if (split[i + 5].isEmpty()) {
                    ((PrimaryEducation) education1).graduate();
                } else {
                    double grade = Double.parseDouble(split[i + 5]);
                    ((GradedEducation) education1).graduate(grade);
                }
            }
            education.add(education1);
        }

        return education;
    }

    private static Education chooseEducation(
            EducationDegree institutionType, String institution, LocalDate enrollmentDate, LocalDate graduationDate) {

        switch (institutionType) {
            case PRIMARY:
                return new PrimaryEducation(institution, enrollmentDate, graduationDate);
            case SECONDARY:
                return new SecondaryEducation(institution, enrollmentDate, graduationDate);
            case DOCTORATE:
            case MASTER:
            case BACHELOR:
                return new HigherEducation(institution, enrollmentDate, graduationDate);
            default:
                throw new IllegalArgumentException("Unrecognized education code.");
        }
    }

    private static Set<SocialInsuranceRecord> parseSocialInsuranceLine(String line) {

        String[] split = line.split(";");

        Set<SocialInsuranceRecord> records = new LinkedHashSet<>();

        if (line.isEmpty()) return records;

        for (int i = 0; i < split.length; i += 3) {

            double amount = Double.parseDouble(split[i]);
            int year = Integer.parseInt(split[i + 1]);
            int month = Integer.parseInt(split[i + 2]);

            records.add(new SocialInsuranceRecord(amount, YearMonth.of(year, month)));
        }

        return records;
    }

}


