package bg.swift.socialsystem;

import bg.swift.socialsystem.education.*;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Controller
public class WebApplication {


    static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("d.M.yyyy");

    private PersonRepository personRepository;
    private final RecordsComparator insuranceComparator;
    private final EducationComparator educationComparator;


    public WebApplication(PersonRepository personRepository, RecordsComparator insuranceComparator, EducationComparator educationComparator) {
        this.personRepository = personRepository;
        this.insuranceComparator = insuranceComparator;
        this.educationComparator = educationComparator;
    }

    @GetMapping("/people")
    public String getPeople(Model model, @RequestParam(value = "firstName", required = false, defaultValue = "0") String firstName,
                            @RequestParam(value = "lastName", required = false, defaultValue = "0") String lastName) {

        List<Person> filteredPeople;

        if (firstName.equals("0") && lastName.equals("0")) {
            filteredPeople = personRepository.findAll();
        } else if (!firstName.equals("0") && lastName.equals("0")) {
            filteredPeople = personRepository.findByFirstName(firstName);
        } else if (firstName.equals("0")) {
            filteredPeople = personRepository.findByLastName(lastName);
        } else {
            filteredPeople = personRepository.findByFirstNameAndLastName(firstName, lastName);
        }
        model.addAttribute("people", filteredPeople);

        return "people";
    }

    @GetMapping("/people/{id}")
    public String getPersonBy(@PathVariable("id") Integer id,
                              Model model) {

        Person person = personRepository.findById(id).get();

        List<Education> sortedEducations = new ArrayList<>(person.getEducations());
        sortedEducations.sort(educationComparator);

        List<SocialInsuranceRecord> sortedInsuranceRecords = new ArrayList<>(person.getSocialInsuranceRecords());
        sortedInsuranceRecords.sort(insuranceComparator);

        model.addAttribute("person", person);
        model.addAttribute("sortedEducations", sortedEducations);
        model.addAttribute("sortedInsuranceRecords", sortedInsuranceRecords);

        return "person_details";
    }

    @PostMapping("/people/{id}")
    public String addNewRecord(@PathVariable("id") Integer id,
                               Model model,
                               double amount, int year, int month
    ) {

        Person person = personRepository.findById(id).get();
        person.addSocialInsuranceRecord(Collections.singleton(new SocialInsuranceRecord(amount, YearMonth.of(year, month))));
        personRepository.save(person);

        List<Education> sortedEducations = new ArrayList<>(person.getEducations());
        sortedEducations.sort(educationComparator);

        List<SocialInsuranceRecord> sortedInsuranceRecords = new ArrayList<>(person.getSocialInsuranceRecords());
        sortedInsuranceRecords.sort(insuranceComparator);

        model.addAttribute("person", person);
        model.addAttribute("sortedEducations", sortedEducations);
        model.addAttribute("sortedInsuranceRecords", sortedInsuranceRecords);

        return "person_details";
    }

    @PostMapping("/people/{id}/education")
    public String addNewEducation(@PathVariable("id") Integer id,
                                  Model model,
                                  String institutionType,
                                  String institutionName,
                                  String enrollmentDate,
                                  String graduationDate,
                                  String isGraduated,
                                  String grade
    ) {

        Person person = personRepository.findById(id).get();

        Education education = chooseEducation(
                EducationDegree.of(institutionType.charAt(0)),
                institutionName,
                LocalDate.parse(enrollmentDate, formatter),
                LocalDate.parse(graduationDate, formatter));

        if (isGraduated.equals("YES")) {
            if (grade.isEmpty()) {
                ((PrimaryEducation) education).graduate();
            } else {
                ((GradedEducation) education).graduate(Double.parseDouble(grade));
            }
        }
        person.setEducation(Collections.singleton(education));
        personRepository.save(person);

        List<Education> sortedEducations = new ArrayList<>(person.getEducations());
        sortedEducations.sort(educationComparator);

        List<SocialInsuranceRecord> sortedInsuranceRecords = new ArrayList<>(person.getSocialInsuranceRecords());
        sortedInsuranceRecords.sort(insuranceComparator);

        model.addAttribute("person", person);
        model.addAttribute("sortedEducations", sortedEducations);
        model.addAttribute("sortedInsuranceRecords", sortedInsuranceRecords);

        return "person_details";
    }

    private static Education chooseEducation(
            EducationDegree institutionType, String institutionName, LocalDate enrollmentDate, LocalDate graduationDate) {

        switch (institutionType) {
            case PRIMARY:
                return new PrimaryEducation(institutionName, enrollmentDate, graduationDate);
            case SECONDARY:
                return new SecondaryEducation(institutionName, enrollmentDate, graduationDate);
            case DOCTORATE:
            case MASTER:
            case BACHELOR:
                return new HigherEducation(institutionName, enrollmentDate, graduationDate);
            default:
                throw new IllegalArgumentException("Unrecognized education code.");
        }
    }

    @GetMapping("/people/newPerson")
    public String addPerson(Model model) {

        return "new_person";
    }

    @PostMapping("/people/newPerson")
    public String addNewPerson(
            Model model,
            String firstName,
            String lastName,
            String gender,
            String height,
            String birthday,
            String country,
            String city,
            String municipality,
            String postcode,
            String streetName,
            String streetNumber
    ) {

        Person person = new Person(firstName, lastName, Gender.of(gender.charAt(0)), Integer.parseInt(height), LocalDate.parse(birthday, formatter));
        person.setAddress(new Address(country, city, municipality, postcode, streetName, streetNumber));

        personRepository.save(person);

        model.addAttribute("person", person);

        return "new_person";
    }
}

