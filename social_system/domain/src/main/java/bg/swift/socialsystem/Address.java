package bg.swift.socialsystem;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Address {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String country;
    private String city;
    private String municipality;
    private String postCode;
    private String streetName;
    private String streetNumber;

    private Address() {
    }

    Address(String country, String city, String municipality, String postcode, String streetName, String streetNumber) {
        this.country = country;
        this.city = city;
        this.municipality = municipality;
        this.postCode = postcode;
        this.streetName = streetName;
        this.streetNumber = streetNumber;
    }

    public Integer getId() {
        return id;
    }

    public String getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getMunicipality() {
        return municipality;
    }

    public String getPostCode() {
        return postCode;
    }

    public String getStreetName() {
        return streetName;
    }

    public String getStreetNumber() {
        return streetNumber;
    }
}

