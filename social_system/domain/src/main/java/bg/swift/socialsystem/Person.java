package bg.swift.socialsystem;

import bg.swift.socialsystem.education.Education;
import bg.swift.socialsystem.education.SecondaryEducation;
import bg.swift.socialsystem.insurance.SocialInsuranceRecord;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.ChronoUnit;
import java.util.*;


@Entity
public class Person {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private static LocalDate EARLIEST_DATE_ALLOWED = LocalDate.of(1900, 1, 1);

    private String firstName;
    private String lastName;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    private int height;
    private LocalDate birthday;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Address address;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Education> educations;
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<SocialInsuranceRecord> socialInsuranceRecords;

    private Person() {
    }

    Person(String firstName, String lastName, Gender gender, int height, LocalDate birthday) {

        validateBirthday(birthday);
        validateName(firstName, "first");
        validateName(lastName, "last");
        validateHeight(height);

        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.height = height;
        this.birthday = birthday;
        this.socialInsuranceRecords = new LinkedHashSet<>();
        this.educations = new LinkedHashSet<>();
    }

    public Optional<Double> getSocialAidAmount() {

        boolean doNotHaveSecondaryEducation = true;

        for (Education e : this.educations) {
            if (e instanceof SecondaryEducation && e.hasGraduated()) {
                doNotHaveSecondaryEducation = false;
                break;
            }
        }

        if (doNotHaveSecondaryEducation) {
            return Optional.empty();
        }

        List<SocialInsuranceRecord> records = new ArrayList<>();

        for (SocialInsuranceRecord r : this.socialInsuranceRecords) {
            if (r.recordIsAfter(YearMonth.now().minusMonths(27))) {
                records.add(r);
            }
        }

        boolean hasNoInsuranceRecordsWithinLast27Months = records.isEmpty();

        if (hasNoInsuranceRecordsWithinLast27Months) {
            return Optional.empty();
        }

        boolean hasBeenInsuredInLast3Months = false;

        for (SocialInsuranceRecord e : records) {
            if (e.recordIsAfter(YearMonth.now().minusMonths(3))) {
                hasBeenInsuredInLast3Months = true;
                break;
            }
        }

        if (hasBeenInsuredInLast3Months) {
            return Optional.empty();
        }

        double sum = 0;

        for (SocialInsuranceRecord r : records) {
            sum += r.getAmount();
        }

        double result = Math.round((sum / 24) * 100.0) / 100.0;

        return Optional.of(result);
    }

    private int getAge() {
        return (int) birthday.until(LocalDate.now(), ChronoUnit.YEARS);
    }

    private boolean isUnderAged() {
        return getAge() < 18;
    }

    public Integer getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Gender getGender() {
        return gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public int getHeight() {
        return height;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    void setEducation(Set<Education> education) {
        this.educations.addAll(education);
    }

    void addSocialInsuranceRecord(Set<SocialInsuranceRecord> record) {
        this.socialInsuranceRecords.addAll(record);
    }

    public Set<Education> getEducations() {
        return educations;
    }

    public Set<SocialInsuranceRecord> getSocialInsuranceRecords() {
        return socialInsuranceRecords;
    }


    private void validateName(String firstName, String propertyName) {
        if (firstName == null || firstName.isEmpty()) {
            throw new IllegalArgumentException("Expected non-empty " + propertyName + " name.");
        }
    }

    private void validateHeight(int height) {
        if (height < 40 || 300 < height) {
            throw new IllegalArgumentException("Expected height is between 40 and 300 cm.");
        }
    }

    private void validateBirthday(LocalDate birthday) {
        if (birthday.isBefore(EARLIEST_DATE_ALLOWED) || birthday.isAfter(LocalDate.now())) {
            throw new IllegalArgumentException("Date of birth is expected to be after 01.01.1900 and before now.");
        }
    }
}

