package bg.swift.socialsystem.education;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.time.LocalDate;

@Entity
public abstract class Education {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private boolean graduated;
    private LocalDate enrollmentDate;
    private LocalDate graduationDate;
    private String institutionName;

    Education() {
    }

    Education(String institutionName, LocalDate enrollmentDate, LocalDate graduationDate) {

        if (graduationDate.isBefore(enrollmentDate)) {
            throw new IllegalArgumentException("Graduation date is expected to be after enrollment date.");
        }

        if (institutionName == null || institutionName.isEmpty()) {
            throw new IllegalArgumentException("Expected non-empty institution name.");
        }

        this.enrollmentDate = enrollmentDate;
        this.graduationDate = graduationDate;
        this.institutionName = institutionName;
    }

    void graduate() {
        if (this.graduationDate.isAfter(LocalDate.now())) {
            throw new IllegalStateException("Graduation date is expected to be a date in the past.");
        }

        this.graduated = true;
    }

    public boolean hasGraduated() {
        return graduated;
    }

    public LocalDate getEnrollmentDate() {
        return enrollmentDate;
    }

    public LocalDate getGraduationDate() {
        return graduationDate;
    }

    public String getInstitutionName() {
        return institutionName;
    }

}
